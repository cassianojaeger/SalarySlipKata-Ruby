require '../src/DataStructure/taxes'

class SalarySlip
	attr_accessor :employee_id, :employee_name, :monthly_gross_salary, :taxes

	def initialize(employee_id=0, employee_name='Cassiano', monthly_gross_salary=1.23, taxes=Taxes.new())
		@employee_id = employee_id
		@employee_name = employee_name
		@monthly_gross_salary = monthly_gross_salary
		@taxes = taxes
	end

	def to_s
		puts '  Employee ID: '+@employee_id.to_s
		puts '  Employee Name: '+@employee_name
		printf '  Monthly Gross Salary: '+'£' ; printf('%.2f',@monthly_gross_salary.to_s)
		printf "\n"+'  National Insurance Contribution: '+'£' ; printf('%.2f',@taxes.national_insurance.to_s)
		printf "\n"+'  Tax Payable: '+'£' ; printf('%.2f', @taxes.tax_payable.to_s)
	end
end