class Taxes
	#TODO: Descutir o uso de classes como estrutura de dados ao invés de por os métodos nela
	attr_accessor :national_insurance, :tax_payable

	def initialize(national_insurance=0, tax_payable=0)
		@national_insurance = national_insurance
		@tax_payable = tax_payable
	end
end