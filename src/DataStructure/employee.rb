class Employee
	attr_accessor :id, :name, :annual_gross_salary

    def initialize(id, name, annual_gross_salary)
	    @id = id
	    @name = name
	    @annual_gross_salary = annual_gross_salary
    end
end