require '../src/DataStructure/taxes'

class EmployeeTributesCalculation
	attr_accessor :annual_gross_salary, :monthly_gross_salary
	MONTHS_PER_YEAR = 12

	NO_TAX = 0
	FIRST_NI_BAND_TAX = 0.12
	SECOND_NI_BAND_TAX = 0.02

	FIRST_TAX_PAYABLE_BAND_TAX = 0.2
	SECOND_TAX_PAYABLE_BAND_TAX = 0.4
	THIRD_TAX_PAYABLE_BAND_TAX = 0.45


	def initialize(annual_gross_salary)
		@annual_gross_salary = annual_gross_salary
		@monthly_gross_salary = annual_gross_salary/MONTHS_PER_YEAR
	end

	def calculateEmployeeTaxes()
		national_insurance = calculateNationalInsurance
		tax_payable = calculateTaxPayable
		taxes = Taxes.new(national_insurance, tax_payable)

		return taxes
	end

	def calculateNationalInsurance()
		national_insurance = []

		basicContribution = calculateBasicNIContributions
		higherContribution = calculateHigherNIContributions

		national_insurance += [basicContribution, higherContribution]

		return national_insurance.reduce(0) {|sum, element| sum + element}
	end

	def calculateBasicNIContributions
		if isBasicContributionApplicable
			return @monthly_gross_salary*FIRST_NI_BAND_TAX
		else
			return NO_TAX
		end
	end

	def isBasicContributionApplicable
		return @annual_gross_salary > 8060
	end

	def calculateHigherNIContributions
		if isHigherContributionApplicable
			@monthly_gross_salary*SECOND_NI_BAND_TAX
		else
			return NO_TAX
		end
	end

	def isHigherContributionApplicable
		return annual_gross_salary > 43001
	end

	def calculateTaxPayable()
		tax_payable = []

		basicRateTax = calculateBasicRateTax
		higherRateTax = calculateHigherRateTax
		additionalRateTax = calculateAdditionalHigherRateTax

		tax_payable += [basicRateTax, higherRateTax, additionalRateTax]

		return tax_payable.reduce(0) { |sum, element| sum + element}
	end

	def calculateBasicRateTax
		if isBasicRateApplicable
			return @monthly_gross_salary*FIRST_TAX_PAYABLE_BAND_TAX
		else
			return NO_TAX
		end
	end

	def isBasicRateApplicable
		return @annual_gross_salary > 11000
	end

	def calculateHigherRateTax
		if isHigherRateApplicable
			return @monthly_gross_salary*SECOND_TAX_PAYABLE_BAND_TAX
		else
			return NO_TAX
		end
	end

	def	isHigherRateApplicable
		return @annual_gross_salary > 43000
	end

	def calculateAdditionalHigherRateTax
		if isAdditionalRateApplicable
			return @monthly_gross_salary*THIRD_TAX_PAYABLE_BAND_TAX
		else
			return NO_TAX
		end
	end

	def isAdditionalRateApplicable
		return @annual_gross_salary >  150000
	end

	private :calculateNationalInsurance, :calculateBasicRateTax, :isBasicRateApplicable,
	        :calculateHigherRateTax, :isHigherRateApplicable, :calculateAdditionalHigherRateTax,
	        :isAdditionalRateApplicable, :calculateTaxPayable, :calculateBasicNIContributions,
	        :calculateHigherNIContributions
end