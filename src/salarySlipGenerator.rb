require '../src/DataStructure/salarySlip'
require '../src/DataStructure/employee'
require '../src/DataStructure/taxes'
require '../src/employeeTributesCalculator'

class SalarySlipGenerator
	MONTH_PER_YEAR = 12

    def generateFor(employee)
		taxes = EmployeeTributesCalculation.new(employee.annual_gross_salary).calculateEmployeeTaxes

		salarySlip = SalarySlip.new(employee.id,
		                            employee.name,
		                            calculateMonthlyGrossSalary(employee.annual_gross_salary),
		                            taxes)
		return salarySlip
    end

    def calculateMonthlyGrossSalary(annual_gross_salary)
	    return annual_gross_salary/MONTH_PER_YEAR
    end
end

# Script Tests
e1 = Employee.new(5, 'cassiano', 18000)
sg = SalarySlipGenerator.new()
ss = sg.generateFor(e1)
ss.to_s