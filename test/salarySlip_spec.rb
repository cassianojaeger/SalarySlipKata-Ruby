require '../src/DataStructure/salarySlip'

describe SalarySlip do
=begin
	First Iteration
=end
	context 'When testing SalarySlip constructor' do
	    it 'After initializing salarySlip with no values
			it should have the default values assigned to each variable' do

		    s1 = SalarySlip.new()
		    expect(s1.employee_id).to(eq(0))
		    expect(s1.employee_name).to(eq('Cassiano'))
		    expect(s1.monthly_gross_salary).to(eq(1.23))
	    end
		it 'After initializing salarySlip with values
	        it should have the values assigned to each variable' do
			s1 = SalarySlip.new(1,'Aurelio', 666)
			expect(s1.employee_id).to(eq(1))
			expect(s1.employee_name).to(eq('Aurelio'))
			expect(s1.monthly_gross_salary).to(eq(666))
		end
	end
end