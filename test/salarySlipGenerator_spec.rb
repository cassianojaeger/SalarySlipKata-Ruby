require '../src/salarySlipGenerator'
require '../src/DataStructure/salarySlip'




describe SalarySlipGenerator do
=begin
	First Iteration
=end
	before do
		@annual_gross_salary = 1040
		@months_per_year= 12
		@employee_name = 'Cassiano'
		@employee_id = 5
		@monthly_gross_salary = @annual_gross_salary/@months_per_year
		# STUB FOR EMPLOYEE
		@employee1 = double('employee')
		allow(@employee1).to(receive(:id)) { @employee_id }
		allow(@employee1).to(receive(:name)) { @employee_name }
		allow(@employee1).to(receive(:annual_gross_salary)) { @annual_gross_salary }
	end

    context 'When testing SalarySlipGenerator monthly gross salary' do
	    it 'After receiving a employee, it should generate a salarySlip with
            gross salary equals to 1/12 of the annual gross salary' do

	        salary_slip_generator = SalarySlipGenerator.new()
	        salary_slip_1 = salary_slip_generator.generateFor(@employee1)

			expect(salary_slip_1.monthly_gross_salary).to(eq(@monthly_gross_salary))
        end
    end

	context 'When testing SalarySlipGenerator taxes' do
		it 'If the employee receives less than 8,060.00 for annual gross salary,
		    charge 0% as national insurance' do
			allow(@employee1).to(receive(:annual_gross_salary)) { 8059 }
			salary_slip_generator = SalarySlipGenerator.new()
			salary_slip_1 = salary_slip_generator.generateFor(@employee1)

			expect(salary_slip_1.taxes.national_insurance).to(eq(0))
		end

		it 'If the employee receives between 8,060.00 - 43,000.00 for annual gross salary,
		    charge 12% as national insurance' do
			allow(@employee1).to(receive(:annual_gross_salary)) { 8061 }
			salary_slip_generator = SalarySlipGenerator.new()
			salary_slip_1 = salary_slip_generator.generateFor(@employee1)

			expect(salary_slip_1.taxes.national_insurance).to(eq((@employee1.annual_gross_salary/12)*0.12))
		end

		it 'If the employee receives more than 43,000.00 for annual gross salary,
		    charge 2% plus the previous band tax as national insurance' do
			allow(@employee1).to(receive(:annual_gross_salary)) { 45000 }
			salary_slip_generator = SalarySlipGenerator.new()
			salary_slip_1 = salary_slip_generator.generateFor(@employee1)

			basic_contribution = (@employee1.annual_gross_salary/12)*0.12
			higher_contribution = (@employee1.annual_gross_salary/12)*0.02

			expect(salary_slip_1.taxes.national_insurance).to(eq(basic_contribution+higher_contribution))
		end

		it 'If the employee receives between 11,000.00 - 43,000.00  from annual gross salary,
			he should pay 20% of his monthly gross salary as tax payable,
			if he receives less than 11,000.00, 0% should be charged' do
			allow(@employee1).to(receive(:annual_gross_salary)) { 15001 }
			salary_slip_generator = SalarySlipGenerator.new()
			salary_slip_1 = salary_slip_generator.generateFor(@employee1)
			expect(salary_slip_1.taxes.tax_payable).to(eq((@employee1.annual_gross_salary/12)*0.2))

			allow(@employee1).to(receive(:annual_gross_salary)) { 10999 }
			salary_slip_2 = salary_slip_generator.generateFor(@employee1)
			expect(salary_slip_2.taxes.tax_payable).to(eq(0))

		end
	end
end