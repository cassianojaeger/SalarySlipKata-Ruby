require '../src/DataStructure/employee'

describe Employee do
=begin
	First Iteration
=end
	context 'When testing employee initialization' do
	    it 'After initializing employee, should be fine to print its fields' do
	        e1 = Employee.new(15, 'Cassiano', 1250)
	        expect(e1.id).to((eq 15))
	        expect(e1.name).to((eq 'Cassiano'))
	        expect(e1.annual_gross_salary).to((eq 1250))
	    end
	end
end